from conan import ConanFile

class NeslabExRecipe(ConanFile):
    name = "neslabex"
    executable = "ds_NeslabEx"
    version = "1.0.11"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/sampleenvironment/neslabex.git"
    description = "NeslabEx device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
