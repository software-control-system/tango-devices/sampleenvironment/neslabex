//+=============================================================================
//
// file :        TempController.h
//
// description : C++ header for the TempController device implementation.
//
// project :     TANGO Device Server
//
// $Author: flanglois $
//
// $Revision: 1.2 $
//
// $Log: not supported by cvs2svn $
// Revision 1.1  2006/07/27 08:53:18  syldup
// initial import
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
#ifndef _INCLUDE_TempController_H
#define _INCLUDE_TempController_H

#include <tango.h>

#define THROW_NOT_ALLOWED(name) Tango::Except::throw_exception((const char*) "OPERATION_NOT_ALLOWED",\
	(const char*) "This feature is not supported by the hardware",\
	(const char*) "#name")

class TempController {

public:
	TempController();
	virtual ~TempController();

	virtual void update_state(Tango::DevState& dev_state,string& dev_status);
	virtual void start();
	virtual void stop();

	virtual string send_cmd(const string& cmd);
	virtual string getVersion();

	virtual long getChannel();
	virtual void setChannel(long channel);

	virtual double getTemperatureSetPoint();
	virtual void   setTemperatureSetPoint(double);

	virtual double getTemperature(long n);

	virtual double getLowTemperatureLimit();
	virtual void   setLowTemperatureLimit(double);

	virtual double getHighTemperatureLimit();
	virtual void   setHighTemperatureLimit(double);

	virtual double getTemperatureError();

	virtual double getHeaterPercent();
	virtual void   setHeaterPercent(double);

	virtual double getHeaterMax();
	virtual void   setHeaterMax(double);

	virtual double getGasFlow();
	virtual void   setGasFlow(double);

	virtual double getPIDIntegralTime();
	virtual void   setPIDIntegralTime(double);

	virtual double getPIDDifferentialTime();
	virtual void   setPIDDifferentialTime(double);

	virtual double getPIDProportionalFactor();
	virtual void   setPIDProportionalFactor(double);

	virtual bool getHeaterAuto();
	virtual void setHeaterAuto(bool);

	virtual bool getPIDAuto();
	virtual void setPIDAuto(bool);

	virtual bool getGasAuto();
	virtual void setGasAuto(bool);

	virtual long getControlSensor();
	virtual void setControlSensor(long);
};

#endif // _INCLUDE_TempController_H
