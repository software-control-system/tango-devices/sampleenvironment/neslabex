//+=============================================================================
//
// file :        EX17.cpp
//
// description : C++ source for the EX17 device implementation.
//
// project :     TANGO Device Server
//
// $Author: flanglois $
//
// $Revision: 1.6 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================

#include "ex17.h"
#include <iomanip>

#define SL_FLUSH_BOTH 2
#define SL_NCHAR 1

/**
 * Constructor
 * @param dev : a pointer to the parent Device
 */
EX17::EX17(Tango::DeviceImpl* dev) 
: Tango::LogAdapter(dev) {
	m_device = dev;
	m_comm_proxy = 0;
	m_version = "Unknown";
	m_lead_char = (char) 0xCA; /* 0xCA = RS-232 */
	m_addr = 0x0001;
	m_second_sensor_enabled = false;
}

/**
 * Destructor
 */
EX17::~EX17() {
	if(m_comm_proxy) {
		delete m_comm_proxy;
		m_comm_proxy = 0;
	}
}

/**
 * Set and try to initialize the comm proxy
 * @param gpib_proxy : Tango name of the proxy
 */
bool EX17::init(const string& comm_proxy_name,int comm_type,unsigned int comm_addr) {
	m_comm_proxy_name = comm_proxy_name;
	if(comm_type==1) {
		// RS-485 lead char
		m_lead_char = 0xCC;
	}
	m_addr = comm_addr;
	// Initialize the comm proxy
	// Note : can throw Tango Exceptions
	bool ok = 0;
	if(m_comm_proxy) {
		delete m_comm_proxy;
		m_comm_proxy = 0;
	}
	m_comm_proxy = new Tango::DeviceProxyHelper(m_comm_proxy_name,m_device);
	if(m_comm_proxy==0) {
		ERROR_STREAM << "Could not initialize communication proxy : '" << m_comm_proxy << "' !" << endl;
	} else {
		INFO_STREAM << "Using communication proxy : '" << m_comm_proxy_name << "'" << endl;
	}

	// Read protocol version (as a communication test)
	unsigned int version = cmd_read_aknowledge();
	unsigned int major = (version <<8) & 0xFF;
	unsigned int minor = version & 0xFF;
	ostringstream oss;
	oss << "Protocol version " << major << "." << minor;
	m_version = oss.str();

	return 1;
}

/**
 * Read the state of the controller
 * @return the new state
 */
void EX17::update_state(Tango::DevState& dev_state,string& dev_status) {
	dev_state = Tango::UNKNOWN;
	dev_status = "Could not get controller status.";
	try 
  {
		ostringstream oss;
		unsigned char out[] = {0x09,0x00};
		unsigned char in[7];
		send(out,2);
		recv(in,7);
		// update state
		unsigned char d4 = in[5];
		if(d4 & 0x20) // 0x20 -> bit 5 de l'octet d4 : "Unit faulted"
    {
			dev_state = Tango::FAULT;
		}
		else if(d4 & 0x80) // 0x80 -> bit 7 de l'octet d4 : "Buzzer On"
    {
			dev_state = Tango::ALARM;
		}
		else if(d4 & 0x08) // 0x08 -> bit 3 de l'octet d4 : "Unit On"
    {
			dev_state = Tango::RUNNING;
		}
    else
    {
			dev_state = Tango::OFF;
		}
		// update status
		if(out[0]==in[0] && in[1]==5) // verifie que l'on a bien recupere le status 
    {
			for(long i=0; i<5; i++)
      {
				
				// get each byte
				unsigned char status_byte = in[i+2]; // +2 pour ignorer les 2 premiers octets
				INFO_STREAM << "status byte num " << i << " = " << std::dec << status_byte << std::dec << endl;
				printf ("status byte = 0x%2x \n",status_byte);

				if(m_second_sensor_enabled && i==1) // i==1 : octet d2
				{
					status_byte &= 0x01; // FL: ?????  &= : Bitwise-AND assignment
					printf ("(apres 2nd sensor, status byte = 0x%x \n",status_byte);

				}
				// decode status byte
				for(long j=0; j<8; j++)
        {
					if(((status_byte) & (2^j)) == (2^j)) 
          {
						const std::string& status = ex17_status[8*i+j];
						INFO_STREAM << "Status: " << status << endl;
						if(status!="")
							oss << status << std::endl;
					}
				}
			}
		}
		dev_status = oss.str();
	}
  catch(...)
  {}
}

/**
 * Start sweeping or holding temperature
 */
void EX17::start() {
	unsigned char cmd[] = { 0x81,0x08,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02 };
	send(cmd,sizeof cmd);
}

/**
 * Stop sweeping or holding temperature
 */
void EX17::stop() {
	unsigned char cmd[] = { 0x81,0x08,0x00,0x02,0x02,0x02,0x02,0x02,0x02,0x02 };
	send(cmd,sizeof cmd);
}

long EX17::send(unsigned char* data,long len) {
	if(!m_comm_proxy) {
		Tango::Except::throw_exception(
			(const char *)"OPERATION_NOT_ALLOWED",
			(const char *)"The communication device proxy was not initialized.",
			(const char *)"EX17::send()");
	}
	long ret = 0;
	Tango::DevVarCharArray buff;
	buff.length(len+4);
	long index = 0;
	buff[index++] = m_lead_char; /* 0xCA = RS-232, 0xCC = RS-485 */
	buff[index++] = (m_addr >> 8) & 0xFF; // >> : bitwise right shift operator
	buff[index++] = m_addr & 0xFF;  // & :  Bitwise-AND Operator
	long checksum = buff[1] + buff[2];
	for(long i=0; i<len; i++) {
		buff[index++] = data[i];
		checksum += data[i];
	}
	checksum ^= 0xFF; // ^= -> Bitwise-exclusive-OR assignment (utilis� pour faire un bitwise inversion)
	checksum &= 0xFF; // &= -> Bitwise-AND assignment
	buff[index++] = checksum;
	try {
		m_comm_proxy->command_in("DevSerFlush",(Tango::DevLong) SL_FLUSH_BOTH);
		m_comm_proxy->command_in("DevSerWriteChar",buff);	
		ret = len;
	}
	catch(Tango::DevFailed& ex) {
		Tango::Except::re_throw_exception(ex,
			(const char *)"HARDWARE_FAILURE",
			(const char *)"Could not write to controller.",
			(const char *)"EX17::send()");
	}
	return ret;
}

long EX17::recv(unsigned char* data,long len) {
	if(!m_comm_proxy) {
		Tango::Except::throw_exception(
			(const char *)"OPERATION_NOT_ALLOWED",
			(const char *)"The communication device proxy was not initialized.",
			(const char *)"EX17::recv()");
	}
	long index = 0;
	if(len>=2) {
		const Tango::DevVarCharArray* buff = 0;

		// read lead char + addr + cmd + data length
		try {
			m_comm_proxy->command_inout("DevSerReadChar",static_cast<Tango::DevLong> (SL_NCHAR + (5 << 8)),buff);
		} catch(Tango::DevFailed& ex) {
			Tango::Except::re_throw_exception(ex,
				(const char *)"HARDWARE_FAILURE",
				(const char *)"Could not read from controller.",
				(const char *)"EX17::recv()");
		}
		if(!buff
		|| buff->length()!=5
		|| (*buff)[0]!=m_lead_char
		|| (*buff)[1]!=((m_addr >> 8) & 0xFF)
		|| (*buff)[2]!=(m_addr & 0xFF)) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "Invalid controller response.",
				(const char*) "EX17::recv()");
		}
		long checksum = (*buff)[1] + (*buff)[2];
		data[index++] = (*buff)[3]; // cmd
		data[index++] = (*buff)[4]; // data length

		// read data + checksum
		long nread = data[1] + 1;
		try {
			m_comm_proxy->command_inout("DevSerReadChar",(Tango::DevLong) (SL_NCHAR + (nread << 8)),buff);
		} catch(Tango::DevFailed& ex) {
			Tango::Except::re_throw_exception(ex,
				(const char *)"HARDWARE_FAILURE",
				(const char *)"Could not read from controller.",
				(const char *)"EX17::recv()");
		}
		if(!buff || buff->length()!=nread) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "Invalid controller response.",
				(const char*) "EX17::recv()");
		}

		// check for BATHERROR REPONSE
		if((*buff)[0]==0x0F && (*buff)[1]==0x02 && (*buff)[2]==0x01) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "The controller returned an error : Bad Command.",
				(const char*) "EX17::recv()");
		}
		if((*buff)[0]==0x0F && (*buff)[1]==0x02 && (*buff)[2]==0x03) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "The controller returned an error : Bad Checksum.",
				(const char*) "EX17::recv()");
		}
		long i;
		// OK, Copy data
		for(i=0; i<(nread - 1) && index<len; i++) {
			data[index++] = (*buff)[i];
		}

		// Verify checksum
		for(i=0; i<index; i++) {
			checksum += data[i];
		}
		checksum ^= 0xFF;
		checksum &= 0xFF;
		if(checksum != (*buff)[nread - 1]) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "Wrong checksum !",
				(const char*) "EX17::recv()");
		}
	}
	return index;
}

unsigned int EX17::cmd_read_aknowledge() {
	unsigned int ret = 0;
	unsigned char cmd = 0x00;
	unsigned char buff[4];
	buff[0] = cmd;
	buff[1] = 0;
	send(buff,2);
	recv(buff,4);
	if(buff[0]==cmd && buff[1]==2) {
		ret = (buff[2] << 8) + buff[3];
	}
	return ret;
}

double EX17::cmd_read_float(unsigned char cmd) {
	double ret = 0.0;
	unsigned char buff[5];
	buff[0] = cmd;
	buff[1] = 0;
	send(buff,2);
	recv(buff,5);
	if(buff[0]==cmd && buff[1]==3) {
		ret = decode_float(buff+2);
	}
	return ret;
}

double EX17::cmd_set_float(unsigned char cmd,double val) {
	double ret = 0.0;
	//short val16 = (val * 10 + 0.5);
	short val16 = IROUND(val*10);
	unsigned char val_h = (val16 >> 8) & 0xFF;
	unsigned char val_l = val16 & 0xFF;
	unsigned char buff[5];
	buff[0] = cmd;
	buff[1] = 2;
	buff[2] = val_h;
	buff[3] = val_l;
	send(buff,4);
	recv(buff,5);
	if(buff[0]==cmd && buff[1]==3) {
		if(buff[3] != val_h || buff[4] != val_l) {
			Tango::Except::throw_exception((const char*) "HARDWARE_FAILURE",
				(const char*) "Could not set this value",
				(const char*) "EX17::cmd_set_float()");
		}
	}
	return ret;
}

double EX17::decode_float(unsigned char* buff) {
	double ret = 0.0;
	short val16 = (buff[1] << 8) + buff[2];
	switch(buff[0]) {
		case 0x10 :
		case 0x11 :
			ret = (double) val16 / 10;
			break;
		case 0x20 :
		case 0x21 :
			ret = (double) val16 / 100;
			break;
	}
	return ret;
}

/**
 * Get the version of the controller
 * @return the version string
 */
string EX17::getVersion() {
	return m_version;
}

/**
 * Read the temperature set point on the controller
 * @return the temperature
 */
double EX17::getTemperatureSetPoint() {
	return cmd_read_float(0x70);
}

/**
 * Write the temperature set point to the controller
 * @param val : the temperature to set
 */
void   EX17::setTemperatureSetPoint(double val) {
	cmd_set_float(0xF0,val);
}

/**
 * Read the measured temperature of a sensor on the controller
 * @param n : the sensor index (1<=n<=2)
 * @return the measured temperature
 */
double EX17::getTemperature(long n) {
	if(n==0) {
		// return internal temperature
		return cmd_read_float(0x20);
	}
	else if(n==1 && m_second_sensor_enabled) {
		// return external sensor
		return cmd_read_float(0x21);
	}
	else {
		THROW_NOT_ALLOWED("EX17::getTemperature()");
	}
	return 0.0;
}

/**
 * Read the measured temperature error on the controller
 * @return the measured temperature error
 */
double EX17::getTemperatureError() {
	return getTemperatureSetPoint() - getTemperature(0);
}

/**
 * Read the temperature low limit
 * @return the temperature low limit
 */
double EX17::getLowTemperatureLimit() {
	return cmd_read_float(0x40);
}

/**
 * Set the temperature low limit
 * @param val : the temperature low limit
 */
void EX17::setLowTemperatureLimit(double val) {
	cmd_set_float(0xC0,val);
}

/**
 * Read the temperature high limit
 * @return the temperature high limit
 */
double EX17::getHighTemperatureLimit() {
	return cmd_read_float(0x60);
}

/**
 * Set the temperature high limit
 * @param val : the temperature high limit
 */
void EX17::setHighTemperatureLimit(double val) {
	cmd_set_float(0xE0,val);
}

/**
 * Read the PID integral time from the controller
 * @return the PID integral time
 */
double EX17::getPIDIntegralTime() {
	return cmd_read_float(0x72);
}

/**
 * Write the PID integral time to the controller
 * @param val : the PID integral time
 */
void   EX17::setPIDIntegralTime(double val) {
	cmd_set_float(0xF2,val*10);
}

/**
 * Read the PID differential time from the controller
 * @return the PID differential time
 */
double EX17::getPIDDifferentialTime() {
	return cmd_read_float(0x73);
}

/**
 * Write the PID differential time to the controller
 * @param val : the PID differential time
 */
void   EX17::setPIDDifferentialTime(double val) {
	cmd_set_float(0xF3,val*10);
}

/**
 * Read the PID proportional factor from the controller
 * @return the PID proportional factor
 */
double EX17::getPIDProportionalFactor() {
	return cmd_read_float(0x71);
}

/**
 * Write the PID proportional factor to the controller
 * @param val : the PID proportional factor
 */
void   EX17::setPIDProportionalFactor(double val) {
	cmd_set_float(0xF1,val);
}

