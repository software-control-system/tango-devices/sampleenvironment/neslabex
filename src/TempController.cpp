//+=============================================================================
//
// file :        TempController.h
//
// description : C++ source for the TempController device implementation.
//
// project :     TANGO Device Server
//
// $Author: dhaussy $
//
// $Revision: 1.2 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================

#include <tango.h>
#include "TempController.h"

TempController::TempController() {
}

TempController::~TempController() {
}

void TempController::update_state(Tango::DevState& state,string& status) {
	THROW_NOT_ALLOWED("TempController::update_state()");
}

void TempController::start() {
	THROW_NOT_ALLOWED("TempController::start()");
}

void TempController::stop() {
	THROW_NOT_ALLOWED("TempController::stop()");
}

string TempController::send_cmd(const string& cmd) {
	THROW_NOT_ALLOWED("TempController::send_cmd()");
	return "";
}

string TempController::getVersion() {
	THROW_NOT_ALLOWED("TempController::getVersion()");
	return "Unknown";
}

long TempController::getChannel() {
	THROW_NOT_ALLOWED("TempController::getChannel()");
	return 0;
}

void TempController::setChannel(long channel) {
	THROW_NOT_ALLOWED("TempController::setChannel()");
}

double TempController::getTemperatureSetPoint() {
	THROW_NOT_ALLOWED("TempController::getTemperatureSetPoint()");
	return 0.0;
}

void   TempController::setTemperatureSetPoint(double) {
	THROW_NOT_ALLOWED("TempController::setTemperatureSetPoint");
}

double TempController::getTemperature(long n) {
	THROW_NOT_ALLOWED("TempController::getTemperature()");
	return 0.0;
}

double TempController::getLowTemperatureLimit() {
	THROW_NOT_ALLOWED("TempController::getLowTemperatureLimit()");
	return 0.0;
}

void   TempController::setLowTemperatureLimit(double) {
	THROW_NOT_ALLOWED("TempController::getLowTemperatureLimit()");
}

double TempController::getHighTemperatureLimit() {
	THROW_NOT_ALLOWED("TempController::getLowTemperatureLimit()");
	return 0.0;
}

void   TempController::setHighTemperatureLimit(double) {
	THROW_NOT_ALLOWED("TempController::getLowTemperatureLimit()");
}

double TempController::getTemperatureError() {
	THROW_NOT_ALLOWED("TempController::getTemperatureError()");
	return 0.0;
}

double TempController::getHeaterPercent() {
	THROW_NOT_ALLOWED("TempController::getHeaterPercent()");
	return 0.0;
}

void   TempController::setHeaterPercent(double) {
	THROW_NOT_ALLOWED("TempController::setHeaterPercent()");
}

double TempController::getHeaterMax() {
	THROW_NOT_ALLOWED("TempController::getHeaterMax()");
	return 0.0;
}

void   TempController::setHeaterMax(double) {
	THROW_NOT_ALLOWED("TempController::setHeaterMax()");
}

double TempController::getGasFlow() {
	THROW_NOT_ALLOWED("TempController::getGasFlow()");
	return 0.0;
}

void   TempController::setGasFlow(double) {
	THROW_NOT_ALLOWED("TempController::setGasFlow()");
}

double TempController::getPIDIntegralTime() {
	THROW_NOT_ALLOWED("TempController::getPIDIntegralTime()");
	return 0.0;
}

void   TempController::setPIDIntegralTime(double) {
	THROW_NOT_ALLOWED("TempController::setPIDIntegralTime()");
}

double TempController::getPIDDifferentialTime() {
	THROW_NOT_ALLOWED("TempController::getPIDDifferentialTime()");
	return 0.0;
}

void   TempController::setPIDDifferentialTime(double) {
	THROW_NOT_ALLOWED("TempController::setPIDDifferentialTime()");
}

double TempController::getPIDProportionalFactor() {
	THROW_NOT_ALLOWED("TempController::getPIDProportionalFactor()");
	return 0.0;
}

void   TempController::setPIDProportionalFactor(double) {
	THROW_NOT_ALLOWED("TempController::setPIDProportionalFactor()");
}

bool TempController::getHeaterAuto() {
	THROW_NOT_ALLOWED("TempController::getHeaterAuto()");
	return false;
}

void TempController::setHeaterAuto(bool) {
	THROW_NOT_ALLOWED("TempController::setHeaterAuto()");
}

bool TempController::getPIDAuto() {
	THROW_NOT_ALLOWED("TempController::getPIDAuto()");
	return 0;
}

void TempController::setPIDAuto(bool) {
	THROW_NOT_ALLOWED("TempController::setPIDAuto()");
}

bool TempController::getGasAuto() {
	THROW_NOT_ALLOWED("TempController::getGasAuto()");
	return false;
}

void TempController::setGasAuto(bool) {
	THROW_NOT_ALLOWED("TempController::setGasAuto()");
}


long TempController::getControlSensor() {
	THROW_NOT_ALLOWED("TempController::getControlSensor()");
	return 0;
}

void TempController::setControlSensor(long) {
	THROW_NOT_ALLOWED("TempController::setControlSensor()");
}
