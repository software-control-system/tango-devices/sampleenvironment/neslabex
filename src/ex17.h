//+=============================================================================
//
// file :        EX17.h
//
// description : C++ header for the EX17 device implementation.
//
// project :     TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2007/05/22 08:46:52  dhaussy
// * PID factors in property
// * second sensor optionnal
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
#ifndef _INCLUDE_LTC11_H
#define _INCLUDE_LTC11_H

#include <math.h>
#include <tango.h>
#include <DeviceProxyHelper.h>
#include "TempController.h"

#define IROUND(X) static_cast<int>(X>=0?(X + 0.5):(X - 0.5))

const string ex17_status[] = {
	// D1.0 - D1.7
	"",
	"",
	"",
	"",
	"Sensor1 Shorted",
	"Sensor1 Open",
	"Sensor1 Shorted Fault",
	"Sensor1 Open Fault",
	// D2.0 - D2.7
	"HTC Fault",
	"Sensor2 Controlling",
	"Sensor2 Shorted",
	"Sensor2 Open",
	"Sensor2 Shorted Warn",
	"Sensor2 Open Warn",
	"Sensor2 Shorted Fault",
	"Sensor2 Open Fault",
	// D3.0 - D3.7
	"Low Level Warn",
	"Low Temp Warn",
	"High Temp Warn",
	"Low Level Fault",
	"Low Temp Fault",
	"High Temp Fault",
	"Low Fixed Temp Fault",
	"High Fixed Temp Fault",
	// D4.0 - D4.7
	"Heater On",
	"",
	"Pump On",
	"Unit On",
	"Unit Stopping",
	"Unit Faulted",
	"Alarm Muted",
	"Buzzer On",
	// D5.0 - D5.7
	"",
	"",
	"",
	"",
	"",
	"Heat LED On",
	"Heat LED Flashing",
	"Sensor2 Controlling",
};

class EX17 : public Tango::LogAdapter, public TempController {

public:
	EX17(Tango::DeviceImpl* dev);
	virtual ~EX17();

	void enable_second_sensor(bool enabled) { m_second_sensor_enabled = enabled; }

	bool init(const string& comm_proxy_name,int comm_type,unsigned int comm_addr);
	void update_state(Tango::DevState& dev_state,string& dev_status);
	void start();
	void stop();

	long send(unsigned char* data,long len);
	long recv(unsigned char* data,long len);
	unsigned int cmd_read_aknowledge();
	double cmd_read_float(unsigned char cmd);
	double cmd_set_float(unsigned char cmd,double val);
	double decode_float(unsigned char* buff);

	/* UNSUPPORTED
	long getChannel();
	void setChannel(long channel);
	*/

	string getVersion();

	double getTemperatureSetPoint();
	void   setTemperatureSetPoint(double);

	double getTemperature(long n);
	double getTemperatureError();

	double getLowTemperatureLimit();
	void   setLowTemperatureLimit(double);

	double getHighTemperatureLimit();
	void   setHighTemperatureLimit(double);

	/* UNSUPPORTED
	double getHeaterPercent();
	void   setHeaterPercent(double);

	double getHeaterMax();
	void   setHeaterMax(double);
	*/

	/* UNSUPPORTED
	double getGasFlow();
	void   setGasFlow(double);
	*/

	double getPIDIntegralTime();
	void   setPIDIntegralTime(double);

	double getPIDDifferentialTime();
	void   setPIDDifferentialTime(double);

	double getPIDProportionalFactor();
	void   setPIDProportionalFactor(double);

	/* UNSUPPORTED
	bool getHeaterAuto();
	void setHeaterAuto(bool);
	*/

	/* UNSUPPORTED
	bool getPIDAuto();
	void setPIDAuto(bool);
	*/

	/* UNSUPPORTED
	bool getGasAuto();
	void setGasAuto(bool);
	*/

	/* UNSUPPORTED
	long getControlSensor();
	void setControlSensor(long);
	*/

protected:
	/** gpib proxy device name */
	string m_comm_proxy_name;
	/** version of the controller (read on init) */
	string m_version;
	/** a pointer the the parent Tango Device */
	Tango::DeviceImpl* m_device;
	/** a pointer to the RS232 or RS485 proxy */
	Tango::DeviceProxyHelper* m_comm_proxy;
	/* Communication frames lead char : 0xCA = RS-232, 0xCC = RS-485 */
	unsigned char m_lead_char;
	/* RS-485 address */
	unsigned int m_addr;
	/* enable / disable the second sensor */
	bool m_second_sensor_enabled;
};

#endif // _INCLUDE_LTC11_H
